export interface JsonPlaceHolder {
    userId:number,
    id:number,
    title:string,
    body:string
}
