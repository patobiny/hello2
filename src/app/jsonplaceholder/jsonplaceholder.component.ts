import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JsonPlaceHolder } from '../interfaces/json-place-holder';
import { JsonPlaceHolderService } from '../json-place-holder.service';


@Component({
  selector: 'app-jsonplaceholder',
  templateUrl: './jsonplaceholder.component.html',
  styleUrls: ['./jsonplaceholder.component.css']
})
export class JsonplaceholderComponent implements OnInit {
  [x: string]: any;

  userId:number;
  id:number;
  title:string;
  body:string;
  jsonData$:Observable<JsonPlaceHolder>
  hasError:boolean = false;

  constructor(private route: ActivatedRoute, private postsService:JsonPlaceHolderService) { }
  

  ngOnInit(): void {
    this.jsonData$ = this.postsService.searchJsonData();
    this.jsonData$.subscribe(
      data => {
                this.userId=data.userId;
                this.id= data.id;
                this.title=data.title;
                this.body= data.body;
              },
      error => {
        console.log(error.message);
        this.hasError=true;
      })
    
}}
