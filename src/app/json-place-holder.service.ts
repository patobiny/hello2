import { throwError, Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JsonPlaceHolder } from './interfaces/json-place-holder';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class JsonPlaceHolderService {

  private URL = "https://jsonplaceholder.typicode.com/posts/";

  constructor(private http:HttpClient) {}

  searchJsonData():Observable<JsonPlaceHolder>{
    return this.http.get<JsonPlaceHolder>(`${this.URL}`).pipe(
      catchError(this.handleError)
    )
  }
  
  private handleError(res:HttpErrorResponse){
    console.log(res.error);
    return throwError(res.error || 'Server erorr'); 
  }

}
